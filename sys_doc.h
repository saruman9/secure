#ifndef _SYS_DOC_H_
#define _SYS_DOC_H_

#define BUF_SIZE    1

/* Set root permission */

void
set_root();

/* Return user permission */

void
set_user(uid_t ruid, gid_t rgid);

/* Collect information about PC and write into file (depend: inxi) */

int
info_pc_file(const char*    file,
             const char*    command);

/* Print usage of program */

void
print_usage(const char*     program);

/* Check GPG key, create test file, sign it, verify it, delete it */

int
check_gpg(char*     key);

/* Set permission, run and set autostart in systemd */

int
run_autostart(char*     path);

#endif /* _SYS_DOC_H_ */
