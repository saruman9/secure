#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <locale.h>

#include <gpgme.h>

#define fail_if_err(err)					\
  do								\
    {								\
      if (err)							\
        {							\
          fprintf (stderr, ": file %s line %d: <%s> %s\n",	\
                   __FILE__, __LINE__, gpgme_strsource (err),	\
		   gpgme_strerror (err));			\
          exit (1);						\
        }							\
    }								\
  while (0)


static const char *
nonnull (const char *s)
{
  return s? s :"[none]";
}


void
print_data_file (gpgme_data_t dh, char* path_file)
{
#define BUF_SIZE 512
  char buf[BUF_SIZE + 1];
  int ret;

  FILE* file;
  if(!(file = fopen(path_file, "wb"))){
      perror("open file");
      exit(EXIT_FAILURE);
  }

  ret = gpgme_data_seek (dh, 0, SEEK_SET);
  if (ret)
    fail_if_err (gpgme_err_code_from_errno (errno));
  while ((ret = gpgme_data_read (dh, buf, BUF_SIZE)) > 0)
    fwrite (buf, ret, 1, file);
  if (ret < 0)
    fail_if_err (gpgme_err_code_from_errno (errno));

  fclose(file);
}


void
init_gpgme (gpgme_protocol_t proto)
{
  gpgme_error_t err;

  gpgme_check_version (NULL);
  setlocale (LC_ALL, "");
  gpgme_set_locale (NULL, LC_CTYPE, setlocale (LC_CTYPE, NULL));

  err = gpgme_engine_check_version (proto);
  fail_if_err (err);
}
