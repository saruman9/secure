#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>

#include "search_gpg.h"

/* Search GPG file in directory, search - name of GPG file */

char*
search_gpg(char*    directory,
           char*    search,
           char*    ret,
           int      level)
{
    if(level > 1 )
        return ret;

    DIR             *dir;
    struct dirent   *entry;

    if(!(dir = opendir(directory))){
        perror("open directory");
        return "";
    }
    if(!(entry = readdir(dir))){
        perror("read directory");
        return "";
    }

    strcpy(ret, directory);
    do{
        if (entry->d_type == DT_DIR) {
            char path[1024];
            int len = snprintf(path, sizeof(path)-1, "%s/%s", directory, entry->d_name);
            path[len] = '\0';
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
                continue;
            ret = search_gpg(path, search, ret, level + 1);
            }
        else if(strstr(entry->d_name, search)){
            sprintf(ret, "%s/%s", directory, entry->d_name);
            closedir(dir);
            return ret;
        }
    } while ((entry = readdir(dir)) && !(strstr(ret, search)));

    closedir(dir);

    return ret;
}
