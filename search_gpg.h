#ifndef _SEARCH_GPG_H_
#define _SEARCH_GPG_H_

/* Search GPG file in directory, search - name of GPG file */

char*
search_gpg(char*    directory,
           char*    search,
           char*    path,
           int      level);

#endif /* _SEARCH_GPG_H_ */
