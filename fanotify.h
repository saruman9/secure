/* File fanotify.h */
#ifndef _FANOTIFY_H_
#define _FANOTIFY_H_

/* Read all available fanotify events from the file descriptor 'fd' */

int
handle_events(int           fd,
              char*   msg_path,
              char*   sig_path);

/* Get program name from PID of process */

static char *
get_program_name_from_pid (int      pid,
                           char     *buffer,
                           size_t   buffer_size);

/* Get path for file that caused the event from fd */

static char *
get_file_path_from_fd (int      fd,
                       char     *buffer,
                       size_t   buffer_size);

/* Copy file for check */
int
copy_file(char*     path_orig,
          char*     path_copy);

#endif /* _FANOTIFY_H_ */

