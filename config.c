#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libconfig.h>
#include "config.h"

/* Read config files from conf_path and return pointer to struct */

struct config *
get_config(const char* conf_path, struct config* conf)
{
  config_t      cfg;
  const char    *name;
  const char    *sys;
  const char    *file;
  int           flash;
  int           dir;
  const char    *mount;
  const char    *path;

  config_init(&cfg);

  /* Read the file. If there is an error, report it and exit. */
  if(! config_read_file(&cfg, conf_path))
  {
    fprintf(stderr, "%s:%d - %s\n", config_error_file(&cfg),
            config_error_line(&cfg), config_error_text(&cfg));
    config_destroy(&cfg);
    return NULL;
  }

  /* Get the GPG filename */

  if(config_lookup_string(&cfg, "name", &name)){
    strcpy(conf->name, name);
  }
  else
    fprintf(stderr, "No 'name' setting in configuration file.\n");

  /* Get the copy of sys.txt filename */

  if(config_lookup_string(&cfg, "sys", &sys)){
    strcpy(conf->sys, sys);
  }
  else
    fprintf(stderr, "No 'sys' setting in configuration file.\n");

  /* Get the path and filename of sys.txt */

  if(config_lookup_string(&cfg, "file", &file)){
    strcpy(conf->file, file);
  }
  else
    fprintf(stderr, "No 'file' setting in configuration file.\n");

  /* Get the flash setting */

  if(config_lookup_int(&cfg, "flash", &flash)){
    conf->flash = flash;
  }
  else
    fprintf(stderr, "No 'flash' setting in configuration file.\n");

  /* Get the dir setting */

  if(config_lookup_int(&cfg, "dir", &dir)){
    conf->dir = dir;
  }
  else
    fprintf(stderr, "No 'dir' setting in configuration file.\n");

  /* Get the mount path */

  if(config_lookup_string(&cfg, "mount", &mount)){
    strcpy(conf->mount, mount);
  }
  else
    fprintf(stderr, "No 'mount' setting in configuration file.\n");

  /* Get the directory path */

  if(config_lookup_string(&cfg, "path", &path)){
    strcpy(conf->path, path);
  }
  else
    fprintf(stderr, "No 'path' setting in configuration file.\n");

  config_destroy(&cfg);
  return conf;
}

/* Write struct to config file */

int
set_config(struct config*   conf,
           char*            path)
{
  config_t cfg;
  config_setting_t *root, *setting, *group, *array;

  config_init(&cfg);
  root = config_root_setting(&cfg);

  /* Add some settings to the configuration. */

  setting = config_setting_add(root, "name", CONFIG_TYPE_STRING);
  config_setting_set_string(setting, conf->name);

  setting = config_setting_add(root, "sys", CONFIG_TYPE_STRING);
  config_setting_set_string(setting, conf->sys);

  setting = config_setting_add(root, "file", CONFIG_TYPE_STRING);
  config_setting_set_string(setting, conf->file);

  setting = config_setting_add(root, "flash", CONFIG_TYPE_INT);
  config_setting_set_int(setting, conf->flash);

  setting = config_setting_add(root, "dir", CONFIG_TYPE_INT);
  config_setting_set_int(setting, conf->dir);

  setting = config_setting_add(root, "mount", CONFIG_TYPE_STRING);
  config_setting_set_string(setting, conf->mount);

  setting = config_setting_add(root, "path", CONFIG_TYPE_STRING);
  config_setting_set_string(setting, conf->path);

  /* Write out the new configuration. */
  if(! config_write_file(&cfg, path))
  {
    fprintf(stderr, "Error while writing config-file.\n");
    config_destroy(&cfg);
    return -1;
  }

  fprintf(stderr, "Конфигурационный файл успешно записан: %s\n",
          path);

  config_destroy(&cfg);
  return 0;
}

/* Write default config to struct config */

struct config*
default_config(struct config* conf){
    strcpy(conf->name, "secure_gpg.sig");
    strcpy(conf->sys, "/tmp/sys.txt_temp");
    strcpy(conf->file, "./sys.txt");
    conf->flash =   1;
    conf->dir   =   1;
    strcpy(conf->mount, "/media");
    strcpy(conf->path, "/home");

    return conf;
}

/* Print config structure in formating view */

int
print_config(struct config* conf){
    printf("Value of name: %s\n", conf->name);
    printf("Value of sys: %s\n", conf->sys);
    printf("Value of file: %s\n", conf->file);
    printf("Value of flash: %d\n", conf->flash);
    printf("Value of dir: %d\n", conf->dir);
    printf("Value of mount: %s\n", conf->mount);
    printf("Value of path: %s\n", conf->path);

    return 0;
}
