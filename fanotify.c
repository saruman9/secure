#define _GNU_SOURCE   /* Needed path_copy get O_LARGEFILE definition */
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/fanotify.h>
#include <unistd.h>
#include <string.h>

#include "fanotify.h"
/* Signing and verification file */
#include "gpg.h"


/* Copy file for check */

int
copy_file(char* path_orig, char* path_copy)
{
    int fd_copy, fd_orig;
    char buf[4096];
    ssize_t nread;
    int saved_errno;

    fd_orig = open(path_orig, O_RDONLY);
    if (fd_orig < 0)
        return -1;

    fd_copy = open(path_copy, O_WRONLY | O_CREAT | O_TRUNC, 0666);
        if (fd_copy < 0)
            goto out_error;

    while (nread = read(fd_orig, buf, sizeof(buf)), nread > 0)
    {
        char *out_ptr = buf;
        ssize_t nwritten;

        do {
            nwritten = write(fd_copy, out_ptr, nread);

            if (nwritten >= 0)
            {
                nread -= nwritten;
                out_ptr += nwritten;
            }
            else if (errno != EINTR)
            {
                goto out_error;
            }
        } while (nread > 0);
    }

    if (nread == 0)
    {
        if (close(fd_copy) < 0)
        {
            fd_copy = -1;
            goto out_error;
        }
    close(fd_orig);

    /* Success!
    * */
    return 0;
    }

    out_error:
        saved_errno = errno;

    close(fd_orig);
    if (fd_copy >= 0)
        close(fd_copy);

    errno = saved_errno;
    return -1;
}

/* Read all available fanotify events from the file descriptor 'fd' */

int
handle_events(int           fd,
              char*   msg_path,
              char*   sig_path)
{
    const struct fanotify_event_metadata    *metadata;
    struct fanotify_event_metadata          buf[200];
    ssize_t                                 len;
    char                                    file_path[PATH_MAX];
    char                                    program_path[PATH_MAX];
    char                                    procfd_path[PATH_MAX];
    struct fanotify_response                response;

    /* Loop while events can be read from fanotify file descriptor */

    for(;;) {

        /* Read some events */

        len = read(fd, (void *) &buf, sizeof(buf));
        if (len == -1 && errno != EAGAIN) {
            perror("read");
            exit(EXIT_FAILURE);
        }

        /* Check if end of available data reached */

        if (len <= 0)
            break;

        /* Point to the first event in the buffer */

        metadata = buf;

        /* Loop over all events in the buffer */

        while (FAN_EVENT_OK(metadata, len)) {

            /* Check that run-time and compile-time structures match */

            if (metadata->vers != FANOTIFY_METADATA_VERSION) {
                fprintf(stderr,
                        "Mismatch of fanotify metadata version.\n");
                exit(EXIT_FAILURE);
            }

            /* metadata->fd contains either FAN_NOFD, indicating a
              queue overflow, or a file descriptor (a nonnegative
              integer). Here, we simply ignore queue overflow. */

            if (metadata->fd >= 0) {

                /* Handle open permission event */

                if (metadata->mask & FAN_OPEN_PERM) {
                    printf("FAN_OPEN_PERM: ");


                    /* Allow file to be opened */
                    if(!(verify_sign(sig_path, msg_path))){
                        response.fd = metadata->fd;
                        response.response = FAN_ALLOW;
                        write(fd, &response,
                         sizeof(struct fanotify_response));
                    } else {
                        response.fd = metadata->fd;
                        response.response = FAN_DENY;
                        write(fd, &response,
                         sizeof(struct fanotify_response));
                    }
                }


                /* Retrieve and print pathname of the accessed file */

                printf("File %s\n", (get_file_path_from_fd(metadata->fd,
                                                    file_path, PATH_MAX) ?
                                    file_path : "unknown"));

                /* Retrieve and print process that caused the event */

                printf("Proccess(PID=%d) %s\n", metadata->pid,
                        (get_program_name_from_pid(metadata->pid,
                                            program_path, PATH_MAX) ?
                            program_path : "unknown"));


                /* Close the file descriptor of the event */

                close(metadata->fd);
            }

            /* Advance to next event */

            metadata = FAN_EVENT_NEXT(metadata, len);
        }
    }
    return 0;
}

/* Get program name from PID of process */

static char *
get_program_name_from_pid (int      pid,
                           char     *buffer,
                           size_t   buffer_size)
{
    int     fd;
    ssize_t len;
    char    *aux;

    /* Try to get program name by PID */

    sprintf (buffer, "/proc/%d/cmdline", pid);
    if ((fd = open (buffer, O_RDONLY)) < 0){
        perror("open fd (PID)");
        return NULL;
    }

    /* Read file contents into buffer */

    if ((len = read (fd, buffer, buffer_size - 1)) <= 0){

        close (fd);
        perror("read fd (PID)");
        return NULL;
    }
    close (fd);

    buffer[len] = '\0';
    aux = strstr (buffer, "^@");
    if (aux)
        *aux = '\0';

    return buffer;
}

/* Get path for file that caused the event from fd */

static char *
get_file_path_from_fd (int      fd,
                       char     *buffer,
                       size_t   buffer_size)
{
    ssize_t   len;

    if (fd <= 0)
        return NULL;

    sprintf (buffer, "/proc/self/fd/%d", fd);
    if ((len = readlink (buffer, buffer, buffer_size - 1)) < 0){
        perror("readlink fd (filepath)");
        return NULL;
    }

    buffer[len] = '\0';
    return buffer;
}
