#ifndef _CONFIG_H_
#define _CONFIG_H_

#include <limits.h>

struct config
{
    char            name[NAME_MAX];
    char            sys[NAME_MAX];
    char            file[PATH_MAX];
    unsigned int    flash;
    unsigned int    dir;
    char            mount[PATH_MAX];
    char            path[PATH_MAX];
};

/* Write default config to struct config */

struct config*
default_config(struct config* conf);

/* Write struct to config file */

int
set_config(struct config*   conf,
           char*            path);

/* Read config files from conf_path and return pointer to struct */

struct config*
get_config(const char* conf_path, struct config* conf);

/* Print config structure in formating view */

int
print_config(struct config* conf);

#endif /* _CONFIG_H_ */

