#define _GNU_SOURCE   /* Needed to get O_LARGEFILE definition */
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/fanotify.h>
#include <unistd.h>
#include <string.h>

/* Check and permission for open file */
#include "fanotify.h"
/* Read and write config file */
#include "config.h"
/* Search GPG file */
#include "search_gpg.h"

int
main(int argc, char *argv[])
{
    /* Variable for fanotify */

    char            buf;
    int             fd;

    /* Variable for config */

    const char*     config_path = "/etc/secure.conf";
    char            sig_path[PATH_MAX];
    char            msg_path[PATH_MAX];
    char            msg_copy_path[PATH_MAX] = "/tmp/";
    struct config   config;
    char*           search_path;

    /* Write default config */

    default_config(&config);

    /* Get configuration from config-file */

    get_config(config_path, &config);
    print_config(&config);

    strcpy(msg_copy_path, config.sys);
    strcpy(msg_path, config.file);

    /* Determinate path for PGP-file */

    if(config.dir){
        strcpy(search_path, config.path);
        printf("%s\n", search_gpg(search_path, config.name, sig_path, 0));
        if(!(strstr(sig_path, config.name)) && config.flash){
            strcpy(search_path, config.mount);
            printf("%s\n", search_gpg(search_path, config.name, sig_path, 0));
        }
    } else if(config.flash){
        strcpy(search_path, config.mount);
        printf("%s\n", search_gpg(search_path, config.name, sig_path, 0));
    } else {
        fprintf(stderr, "Empty path to GPG in config\n");
        exit(EXIT_FAILURE);
    }

    if(!(strstr(sig_path, config.name))){
        fprintf(stderr, "Not found GPG file\n");
        sprintf(sig_path, "%s/%s", config.path, config.name);
        fprintf(stderr, "Search in default dir\n");
    }

    /* Copy file for later check */

    if(copy_file(msg_path, msg_copy_path)){
        perror("copy file");
        exit(EXIT_FAILURE);
    }

    /* Create the file descriptor for accessing the fanotify API */

    fd = fanotify_init(FAN_CLOEXEC | FAN_CLASS_CONTENT | FAN_NONBLOCK,
                      O_RDONLY | O_LARGEFILE);
    if (fd == -1) {
        perror("fanotify_init");
        exit(EXIT_FAILURE);
    }

    /* Mark the mount for:
      - permission events before opening files */

    if (fanotify_mark(fd, FAN_MARK_ADD,
                     FAN_OPEN_PERM, AT_FDCWD,
                     msg_path) == -1) {
        perror("fanotify_mark");
        exit(EXIT_FAILURE);
    }

    /* This is the loop to wait for incoming events */

    while (1) {
        handle_events(fd, msg_copy_path, sig_path);
    }

    exit(EXIT_SUCCESS);
}
