#ifndef _GPG_H_
#define _GPG_H_

/* Verification of signing file */

int
verify_sign (char* path_sign, char* path_msg);

/* Signing file */

int
sign(char *path_in, char *path_out, char *key);

#endif /* _GPG_H_ */

