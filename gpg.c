#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <gpgme.h>
#include "run-support.h"
#include "gpg.h"

static void
print_summary (gpgme_sigsum_t summary)
{
  if ( (summary & GPGME_SIGSUM_VALID      ))
    fputs (" valid", stdout);
  if ( (summary & GPGME_SIGSUM_GREEN      ))
    fputs (" green", stdout);
  if ( (summary & GPGME_SIGSUM_RED        ))
    fputs (" red", stdout);
  if ( (summary & GPGME_SIGSUM_KEY_REVOKED))
    fputs (" revoked", stdout);
  if ( (summary & GPGME_SIGSUM_KEY_EXPIRED))
    fputs (" key-expired", stdout);
  if ( (summary & GPGME_SIGSUM_SIG_EXPIRED))
    fputs (" sig-expired", stdout);
  if ( (summary & GPGME_SIGSUM_KEY_MISSING))
    fputs (" key-missing", stdout);
  if ( (summary & GPGME_SIGSUM_CRL_MISSING))
    fputs (" crl-missing", stdout);
  if ( (summary & GPGME_SIGSUM_CRL_TOO_OLD))
    fputs (" crl-too-old", stdout);
  if ( (summary & GPGME_SIGSUM_BAD_POLICY ))
    fputs (" bad-policy", stdout);
  if ( (summary & GPGME_SIGSUM_SYS_ERROR  ))
    fputs (" sys-error", stdout);

  fputs("\n", stdout);
}

int
verify_sign (char* path_sign, char* path_msg)
{
    gpgme_error_t             err;
    gpgme_ctx_t               ctx;
    gpgme_protocol_t          protocol = GPGME_PROTOCOL_OpenPGP;
    FILE                      *fp_sig = NULL;
    gpgme_data_t              sig = NULL;
    FILE                      *fp_msg = NULL;
    gpgme_data_t              msg = NULL;
    gpgme_verify_result_t     result;
    gpgme_sigsum_t            sig_err;

    fp_sig = fopen (path_sign, "rb");
    if (!fp_sig){
        err = gpgme_error_from_syserror ();
        fprintf (stderr, "can't open '%s': %s\n", path_sign, gpgme_strerror (err));
        return 1;
    }

    fp_msg = fopen (path_msg, "rb");
    if (!fp_msg){
        err = gpgme_error_from_syserror ();
        fprintf (stderr, "can't open '%s': %s\n", path_msg, gpgme_strerror (err));
        return 1;
    }

    init_gpgme (protocol);

    err = gpgme_new (&ctx);
    fail_if_err (err);
    gpgme_set_protocol (ctx, protocol);

    err = gpgme_data_new_from_stream (&sig, fp_sig);
    if (err){
        fprintf (stderr, "error allocating data object: %s\n", gpgme_strerror (err));
        return 1;
    }
    err = gpgme_data_new_from_stream (&msg, fp_msg);
    if (err){
        fprintf (stderr, "error allocating data object: %s\n", gpgme_strerror (err));
        return 1;
    }

    err = gpgme_op_verify (ctx, sig, msg, NULL);
    result = gpgme_op_verify_result (ctx);
    if (err){
        fprintf (stderr, "signing failed: %s\n", gpgme_strerror (err));
        return 1;
    }
    sig_err = result->signatures->summary;

    gpgme_data_release (msg);
    gpgme_data_release (sig);

    gpgme_release (ctx);

    fclose(fp_sig);
    fclose(fp_msg);

    if ((sig_err & GPGME_SIGSUM_VALID))
        return 0;

    print_summary(sig_err);
    return 1;
}

int
sign(char *path_in, char *path_out, char *key)
{
    gpgme_error_t         err;
    gpgme_ctx_t           ctx;
    const char            *key_string = key;
    gpgme_protocol_t      protocol = GPGME_PROTOCOL_OpenPGP;
    gpgme_sig_mode_t      sigmode = GPGME_SIG_MODE_DETACH;
    gpgme_data_t          in;
    gpgme_data_t          out;
    gpgme_sign_result_t   result;

    init_gpgme (protocol);

    err = gpgme_new (&ctx);
    fail_if_err (err);
    gpgme_set_protocol (ctx, protocol);
    gpgme_set_armor (ctx, 1);

    if (key_string){

        gpgme_key_t akey;

        err = gpgme_get_key (ctx, key_string, &akey, 1);
        if (err){
            fprintf(stderr, "error in a key\n");
            return -1;
        }

        err = gpgme_signers_add (ctx, akey);
        fail_if_err (err);
        gpgme_key_unref (akey);
    }

    err = gpgme_data_new_from_file (&in, path_in, 1);
    if (err){
        fprintf (stderr, "file for sign error reading '%s': %s\n",
        path_in, gpgme_strerror (err));
        return -1;
    }

    err = gpgme_data_new (&out);
    fail_if_err (err);

    err = gpgme_op_sign (ctx, in, out, sigmode);
    result = gpgme_op_sign_result (ctx);
    if (err){
        fprintf (stderr, "signing failed: %s\n", gpgme_strerror (err));
        return -1;
    }

    print_data_file (out, path_out);
    gpgme_data_release (out);

    gpgme_data_release (in);

    gpgme_release (ctx);

    return 0;
}
