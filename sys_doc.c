#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <limits.h>
#include <sys/stat.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>

#include "sys_doc.h"
/* GPG sign and verify */
#include "gpg.h"
/* Config file */
#include "config.h"

static uid_t ruid;
static gid_t rgid;

int
main(int argc, char *argv[])
{
    /* Var for option */

    int             option;
    char            *file_info = "sys.txt";
    char            *command = "inxi -v 7 -Fdflmopux -t cm -c 0";
    char            *path;
    int             gen_key = 0;
    char            *key = NULL;
    char            *path_sig = "./";
    char            *name_sig = "secure_gpg.sig";
    char            full_sig[PATH_MAX];
    char            *config = "/etc/secure.conf";
    struct config   conf;

    char        *command_gen_key = "gpg2 --full-gen-key";

    /* Get user ID and group ID of user */

    ruid = getuid();
    rgid = getgid();


    if(argc < 2){
        print_usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    /* Read option */
    while((option = getopt(argc, argv, "f:i:gk:s:n:h")) != -1){
        switch(option){
            case 'f':
                if(optarg[0] == '-'){
                    printf("-f требует аргумента\n");
                    print_usage(argv[0]);
                    exit(EXIT_FAILURE);
                }
                file_info = optarg;
                break;
            case 'i':
                if(optarg[0] == '-'){
                    printf("-i требует аргумента\n");
                    print_usage(argv[0]);
                    exit(EXIT_FAILURE);
                }
                command = optarg;
                break;
            case 'g':
                gen_key = 1;
                break;
            case 'k':
                if(optarg[0] == '-'){
                    printf("-k требует аргумента\n");
                    print_usage(argv[0]);
                    exit(EXIT_FAILURE);
                }
                key = optarg;
                break;
            case 's':
                if(optarg[0] == '-'){
                    printf("-s требует аргумента\n");
                    print_usage(argv[0]);
                    exit(EXIT_FAILURE);
                }
                path_sig = optarg;
                break;
            case 'n':
                if(optarg[0] == '-'){
                    printf("-n требует аргумента\n");
                    print_usage(argv[0]);
                    exit(EXIT_FAILURE);
                }
                name_sig = optarg;
                break;
            case 'h':
                print_usage(argv[0]);
                exit(EXIT_SUCCESS);
            default:
                print_usage(argv[0]);
                exit(EXIT_FAILURE);
        }
    }

    if (optind >= argc) {
        fprintf(stderr, "Укажите путь до папки\n");
        print_usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    /* Check option of GPG */

    if(gen_key && !(key)){
        fprintf(stderr, "Вы должны указать ключ (e-mail)\n");
        print_usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    /* Generate key if option -g */

    if(gen_key){
        if(system(command_gen_key)){
            fprintf(stderr, "Error generate key\n");
            exit(EXIT_FAILURE);
        }
    }


    /* Check GPG */

    if(check_gpg(key))
        exit(EXIT_FAILURE);
    else
        printf("Проверка ключа прошла успешно\n");


    /* Check path */

    if(!(argv[optind])){
        fprintf(stderr, "Не указан путь до папки\n");
        print_usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    /* Change directory to path or create new directory  and copy secure */

    path = argv[optind];
    char temp[PATH_MAX];
    DIR* directory = opendir(path);
    sprintf(temp, "cp secure %s", path);
    if(directory){
        closedir(directory);
        system(temp);
        chdir(path);
    } else {
        if(mkdir(path, 0755)){
            perror("Error mkdir");
            exit(EXIT_FAILURE);
        }
        closedir(directory);
        system(temp);
        chdir(path);
    }


    /* Collect info */

    if(info_pc_file(file_info, command)){
        fprintf(stderr, "error collect info about pc");
        exit(EXIT_FAILURE);
    }

    /* Sign info file */

    sprintf(full_sig, "%s/%s", path_sig, name_sig);

    if(sign(file_info, full_sig, key)){
        fprintf(stderr, "Ошибка подписывания файла");
        exit(EXIT_FAILURE);
    }

    /* Write config file */

    default_config(&conf);

    strcpy(conf.name, name_sig);
    strcpy(conf.file, file_info);
    strcpy(conf.path, path_sig);

    if(set_config(&conf, config)){
       fprintf(stderr, "Ошибка записи конфигурационного файла");
       exit(EXIT_FAILURE);
    }

    /* Run secure and autostart */

    if(run_autostart(path)){
        fprintf(stderr, "Error run and autostart");
        exit(EXIT_FAILURE);
    }

    exit(EXIT_SUCCESS);
}

/* Set root permission */

void
set_root()
{
    if(setuid(0)){
        perror("You should be ROOT");
        exit(EXIT_FAILURE);
    }

    if(setgid(0)){
        perror("setgid");
        exit(EXIT_FAILURE);
    }
}

/* Return user permission */

void
set_user(uid_t ruid, gid_t rgid)
{
    if(setgid(rgid)){
        perror("setgid");
        exit(EXIT_FAILURE);
    }

    if(setuid(ruid)){
        perror("setuid");
        exit(EXIT_FAILURE);
    }
}

/* Print usage of program */

void
print_usage(const char*     program){
    printf("\
    Использование: %s [КЛЮЧ] ПУТЬ\n\
            \n\
        -f [ИМЯ ФАЙЛА]      Имя файла для сбора информации, по умолчанию sys.txt\n\
        -i [ИМЯ ПРОГРАММЫ]  Сторонняя программа для сбора информации, например lsusb\n\
        -g                  Сгенерировать новый ключ пользователя\n\
                            (в качестве аргумента -k следует указать e-mail пользователя)\n\
        -k [E-MAIL]         Использовать для подписи ключ не по умолчанию,\n\
                            в качестве аргумента e-mail пользователя ключа\n\
        -s [ПУТЬ]           Путь, куда сохраняется подпись, либо путь до FLASH-носителя\n\
        -n [ИМЯ]            Имя файла подписи\n\
        -h                  Вывод этого сообщения\n\
\n\
    Программа собирает информацию о компьютере и записывает её в файл. Затем подписывает данный файл и запускает защиту на открытие файла.\n",
            program);
}


int
info_pc_file(const char*    file,
             const char*    command)
{
    FILE        *fd_sys;
    FILE        *fd_inxi;
    char        buf[BUF_SIZE + 1];
    size_t      nread, nwrite;

    if(!(fd_inxi = popen(command, "r"))){
        perror("popen");
        return 1;
    }

    if(!(fd_sys = fopen(file, "wb"))){
        perror("open file sys.txt");
        return 1;
    }

    while((nread = fread(buf, BUF_SIZE, 1, fd_inxi)) > 0)
        nwrite = fwrite(buf, nread, 1, fd_sys);

    if(pclose(fd_inxi)){
        perror("pclose");
        return 1;
    }

    if(fclose(fd_sys)){
        perror("fclose");
        return 1;
    }
    return 0;
}

/* Check GPG key, create test file, sign it, verify it, delete it */

int
check_gpg(char*     key)
{
    /* Create test file for signing and verify */

    if(system("touch test")){
        fprintf(stderr, "Error touch file\n");
        return -1;
    }

    /* Signing test file */

    if(sign("test", "test.sig", key)){
        fprintf(stderr, "Error test signing\nПроверьте правильность ключа\n");
        return -2;
    }

    /* Verify sign and test file */

    if(verify_sign("test.sig", "test")){
        fprintf(stderr, "Error test verify sign\nПроверьте правильность ключа\n");
        return -3;
    }

    set_root();

    /* Delete test file and sign */

    if(system("rm test test.sig")){
        fprintf(stderr, "Error delete test files\n");
        return -4;
    }

    return 0;
}

/* Set permission, run and set autostart in systemd */

int
run_autostart(char*     path)
{
    char        *chmod = "chmod u+s secure";
    FILE        *fd_service;
    char        *path_systemd = "/etc/systemd/user/secure.service";
    char        *run = "systemctl --user start secure.service";
    char        *autostart = "systemctl --user enable secure.service";

    /* Set permission */

    system(chmod);

    /* Create and write systemd.unit */

    if(!(fd_service = fopen(path_systemd, "wb"))){
        perror("create service file");
        return 1;
    }

    fprintf(fd_service,\
"[Unit]\n\
Description=Secure Daemon\n\
\n\
[Service]\n\
ExecStart=%s/secure\n\
ExecStop=/usr/bin/killall secure\n\
WorkingDirectory=%s\n\
\n\
[Install]\n\
WantedBy=default.target\n",\
            path, path);

    if(fclose(fd_service)){
        perror("Error close fd service");
        return 1;
    }

    /* Run and enable autostart */

    set_user(ruid, rgid);

    system(run);
    system(autostart);

    return 0;
}
